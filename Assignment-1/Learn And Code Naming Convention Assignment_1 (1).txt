Assignment-1: Naming Conventions

Assignment 1: The below program is to Roll the Dice


import random
def generateRandomNumber(maximumNumber):
    randomRolledNumber=random.randint(1, maximumNumber)
    return randomRolledNumber

def main():
    maximumNumber=6
    conditionToRollDice=True
    while conditionToRollDice:
        userInput=input("Ready to roll? Enter Q to Quit")
        if userInput.lower() !="q":
            randomRolledNumber=generateRandomNumber(maximumNumber)
            print("You have rolled a",randomRolledNumber)
        else:
            conditionToRollDice=False





Assignment 2: The below program is to guess the correct number between 1 to 100

def checkForNumber(number):
    if number.isdigit() and 1<= int(number) <=100:
        return True
    else:
        return False

def main():
    randomNumber=random.randint(1,100)
    conditionToRepeatAgain=False
    userInputNumber=input("Guess a number between 1 and 100:")
    incrementNumber=0
    while not conditionToRepeatAgain:
        if not checkForNumber(userInputNumber):
            userInputNumber=input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            incrementNumber+=1
            userInputNumber=int(userInputNumber)

        if userInputNumber<randomNumber:
            userInputNumber=input("Too low. Guess again")
        elif userInputNumber>randomNumber:
            userInputNumber=input("Too High. Guess again")
        else:
            print("You guessed it in",incrementNumber,"guesses!")
            conditionToRepeatAgain=True


main()



Assignment 3: The below program is to check whether the number is Armstrong number or not


def checkForArmstrongNumber(inputNumber):
    # Initializing Sum and Number of Digits
    sumOfDigits = 0
    numberOfDigits = 0

    # Calculating Number of individual digits
    temporaryNumber = inputNumber
    while temporaryNumber > 0:
        numberOfDigits = numberOfDigits + 1
        temporaryNumber = temporaryNumber // 10

    # Finding Armstrong Number
    temporaryNumber = inputNumber
    for value in range(1, temporaryNumber + 1):
        remainderOfNumber = temporaryNumber % 10
        sumOfDigits = sumOfDigits + (remainderOfNumber ** numberOfDigits)
        temporaryNumber //= 10
    return sumOfDigits


# End of Function

# User Input
userInputNumber = int(input("\nPlease Enter the Number to Check for Armstrong: "))

if (userInputNumber == checkForArmstrongNumber(userInputNumber)):
    print("\n %d is Armstrong Number.\n" % userInputNumber)
else:
    print("\n %d is Not a Armstrong Number.\n" % userInputNumber)




Assignment 4: Selection Sort


function sort(arrayOfNumbers) 
{

  for (let currentOuterLoopIndex = 0; currentOuterLoopIndex < arrayOfNumbers.length; currentOuterLoopIndex++) 
 {
// Set default active minimum to current index.

    let minimumIndex = currentOuterLoopIndex;
    
// Loop to array from the current value.

    for (let currentInnerLoopIndex = currentOuterLoopIndex + 1; currentInnerLoopIndex < arrayOfNumbers.length; currentInnerLoopIndex++)
    {
    
// If you find an item smaller than the current active minimum,
// make the new item the new active minimum.

      if (arrayOfNumbers[currentInnerLoopIndex] < arrayOfNumbers[minimumIndex ]) 
      {
        minimumIndex = currentInnerLoopIndex;
      }
      
// Keep on looping until you've looped over all the items in the array
// in order to find values smaller than the current active minimum.

    }

// If the current index isn't equal to the active minimum value's index anymore
// swap these two elements.

      if (currentOuterLoopIndex !== minimumIndex) 
      {
        [arrayOfNumbers[currentOuterLoopIndex], arrayOfNumbers[minimumIndex]] = [arrayOfNumbers[minimumIndex], arrayOfNumbers[currentOuterLoopIndex]];
      }

  }

  return arrayOfNumbers;

}


Assignment 5: Find largest and smallest number from an array



import java.util.Arrays;
public class MaximumAndMinimumNumberInArray
{
 
    public static void main(String args[]) 
   {
        findLargestAndSmallestNumber(new int[]{-20, 34, 21, -87, 92,
                             Integer.MAX_VALUE});
        findLargestAndSmallestNumber(new int[]{10, Integer.MIN_VALUE, -2});
        findLargestAndSmallestNumber(new int[]{Integer.MAX_VALUE, 40,
                             Integer.MAX_VALUE});
        findLargestAndSmallestNumber(new int[]{1, -1, 0});
    }
 
    public static void findLargestAndSmallestNumber(int[] numbers)
 {
        int largestNumber = Integer.MIN_VALUE;
        int smallestNumber = Integer.MAX_VALUE;
        for (int number : numbers) 
       {
            if (number > largestNumber)
            {
                largestNumber = number;
            } 
           else if (number < smallestNumber) 
            {
                smallestNumber = number;
            }
        }
 
        System.out.println("Given integer array : " + Arrays.toString(numbers));
        System.out.println("Largest number in array is : " + largestNumber );
        System.out.println("Smallest number in array is : " + smallestNumber);
 }

}



Assignment 6: Find the floor of the expected value(mean) of the subarray from Left to Right.


/*
You are given an array of n numbers and q queries. For each query you have to print the floor of the expected value(mean) of the subarray from L to R.
Inputs
First line contains two integers N and Q denoting number of array elements and number of queries.
Next line contains N space seperated integers denoting array elements.
Next Q lines contain two integers L and R(indices of the array).
Output
print a single integer denoting the answer.
*/


using System;
using System.Numerics;
class MeanOfSubArray 
{
    static void Main(string[] args)
 {
       var numberOfArrayElementsAndQueries = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            var arrayValues = Array.ConvertAll(Console.ReadLine().Split(' '), long.Parse);
            long[] valueOfArrayElements = new long[numberOfArrayElementsAndQueries[0] + 1];
            valueOfArrayElements[0] = 0;
            for (int arrayElement = 1; arrayElement <= numberOfArrayElementsAndQueries[0]; arrayElement++)
            {
                valueOfArrayElements[arrayElement] = valueOfArrayElements[arrayElement - 1] + arrayValues[arrayElement - 1];
            }
            for (var query=0; query< numberOfArrayElementsAndQueries[1]; query ++)
            {
                var currentQueryArrayValues = Array.ConvertAll(Console.ReadLine().Split(' '), int);
                Console.WriteLine((long)((long)(valueOfArrayElements[currentQueryArrayValues[1]] - valueOfArrayElements[currentQueryArrayValues[0] - 1]) / (currentQueryArrayValues[1] - currentQueryArrayValues[0] + 1)));
            }
  }

}


 
/* Example 
Input
5 3
1 2 3 4 5
1 3
2 4
2 5
Output2
3
3*/
