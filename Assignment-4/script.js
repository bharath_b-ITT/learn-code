var numberOfPosts;
var serialnumber = 1;

function getDataFromAPI () {
  var initialPostValue = parseInt(document.getElementById('initialPostValue').value);
  var lastPostValue = parseInt(document.getElementById('lastPostValue').value);
  var blogName = document.getElementById('blogName').value;
  numberOfPosts = lastPostValue - initialPostValue;
  const numberOfBatches = parseInt((lastPostValue - initialPostValue) / 50);
  for (let i = 0; i < numberOfBatches; i++) {
    fetchBatchData(
      blogName,
      initialPostValue + i * 50,
      initialPostValue + (i + 1) * 50
    );
  }
  if ((lastPostValue - initialPostValue) % 50 !== 0) {
    fetchBatchData(
      blogName,
      initialPostValue + numberOfBatches * 50,
      lastPostValue
    );
  }
}

function fetchBatchData (blogName, startingPostValue, endingPostValue) {
  const generatedURL = generatingURL(
    blogName,
    startingPostValue,
    endingPostValue
  );
  fetch(generatedURL)
    .then((response) => response.text())
    .then((blogData) => {
      blogData = blogData.replace('var tumblr_api_read = ', '');
      while (blogData.includes(';')) {
        blogData = blogData.replace(';', '');
      }
      blogData = convertToJson(blogData);
      displayFetchedData(blogData);
    })
    .catch(function () {
      document.getElementById('error').innerHTML = 'No Post Available';
    });
}

function generatingURL (blogName, initialPostValue, lastPostValue) {
  /* Using a CORS proxy to get around “No Access-Control-Allow-Origin header”
  problems to fetch data in my localhost. */
  const mandatoryURL = 'https://cors-anywhere.herokuapp.com/';
  const originalURL =
    'https://' +
    blogName +
    '.tumblr.com/api/read/json?type=photo&num=' +
    lastPostValue +
    '&start=' +
    initialPostValue;
  return mandatoryURL + originalURL;
}

function convertToJson (data) {
  var jsonData;
  jsonData = JSON.parse(data);
  return jsonData;
}

function displayFetchedData (blogData) {
  document.getElementById('title').innerHTML =
    'Title of Post:' + blogData.tumblelog.title;
  document.getElementById('name').innerHTML =
    'Name of Post:' + blogData.tumblelog.name;
  document.getElementById('description').innerHTML =
    'Description of Post:' + blogData.tumblelog.description;
  document.getElementById('totalPosts').innerHTML =
    'Number Of Posts:' + blogData['posts-total'];
  dispalyingUrlOfImages(blogData);
}

function dispalyingUrlOfImages (blogData) {
  for (const post of blogData.posts) {
    var ul = document.getElementById('imagesUrlPost');
    var li = document.createElement('li');
    li.appendChild(
      document.createTextNode(serialnumber + '.' + post['photo-url-1280'])
    );
    ul.appendChild(li);
    serialnumber++;
    if (serialnumber - 1 === numberOfPosts) break;
  }
}
