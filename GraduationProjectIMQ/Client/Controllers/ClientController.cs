using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using IMQ.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class ClientController
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IConfiguration config;
        private ClientService clientService;
        private PublisherService publisherService;
        private SubscriberService subscriberService;
        private NetworkHandler networkHandler;
        private ClientConstants clientConstants;
        private string clientName;
        private string clientRole;
        private string password;
        public ClientController()
        {
            clientConstants = new ClientConstants();
            networkHandler = new NetworkHandler();
            clientService = new ClientService();
            publisherService = new PublisherService();
            subscriberService = new SubscriberService();
            client = networkHandler.intiliazeClientNetwork();
            networkHandler.createNetworkStreams();
        }
        public void connectToServer()
        {
            try
            {
                System.Console.WriteLine("....IMQ....");
            start:
                this.getUserInputForAuthentication();
                bool isAuthenticated = clientService.checkClientAuthentication(networkHandler, clientName, clientRole, password);
                if (isAuthenticated)
                {
                    while (true)
                    {
                        if (clientRole == clientConstants.publisher)
                        {
                            this.getTopics();
                            try
                            {
                                System.Console.WriteLine("\n\nSelect Below Options: \n 1 PublishMessage     2 Create Topic \n");
                                var publisherInput = int.Parse(System.Console.ReadLine());

                                switch (publisherInput)
                                {
                                    case 1:
                                        this.publishMessage();
                                        break;
                                    case 2:
                                        this.createTopic();
                                        break;
                                    default:
                                        Console.WriteLine("please select correct options");
                                        break;
                                }
                            }
                            catch
                            {
                                System.Console.WriteLine("\nInvalid Input\n");
                            }
                        }
                        else
                        {
                            this.getTopics();
                            this.getSubscribedTopics();
                            try
                            {
                                System.Console.WriteLine("\n\nSelect Below Options : \n 1 PullMessages       2 Subscribe to Topic \n");
                                var subscriberInput = int.Parse(System.Console.ReadLine());
                                switch (subscriberInput)
                                {
                                    case 1:
                                        this.getPulledMessages();
                                        break;
                                    case 2:
                                        this.subscribeToTopic();
                                        break;
                                    default:
                                        Console.WriteLine("please select correct options");
                                        break;
                                }
                            }
                            catch
                            {
                                System.Console.WriteLine("\nInvalid Input\n");
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        System.Console.WriteLine("\nLogin Failed Please select Below Options\n");
                        System.Console.WriteLine("1 SignIn Again       2 SignUp \n");
                        var clientInput = int.Parse(System.Console.ReadLine());
                        switch (clientInput)
                        {
                            case 1:
                                goto start;
                            case 2:
                                this.createClient();
                                goto start;
                            default:
                                Console.WriteLine("please select correct options");
                                break;
                        }
                    }
                    catch
                    {
                        System.Console.WriteLine("\nInvalid Input\n");
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e);
                networkHandler.closeConnection();
            }

        }

        private void getUserInputForAuthentication()
        {
            System.Console.WriteLine("Login");
            System.Console.WriteLine("Please Enter name ");
            clientName = System.Console.ReadLine();
            System.Console.WriteLine("Please Select the Role ");
            System.Console.WriteLine("1 Publisher       2 Subscriber \n");
            var roleId = int.Parse(System.Console.ReadLine());
            clientRole = roleId == 1 ? clientConstants.publisher : clientConstants.subscriber;
            System.Console.WriteLine("Please Enter password");
            password = System.Console.ReadLine();
        }

        private void getTopics()
        {
            try
            {
                Console.WriteLine("\n\nAvailable Topics");
                Queue<string> allTopics = clientService.getAllTopics(networkHandler);
                foreach (var topic in allTopics)
                {
                    Console.WriteLine("{0}", topic);
                }
            }
            catch
            {
                Console.WriteLine("Not able to get all the topics");
            }
        }

        private void getSubscribedTopics()
        {
            try
            {
                Console.WriteLine("\n\nTopics you subscribed");
                Queue<string> topicsSubscribed = subscriberService.getTopicsWhichUserSubscribed(networkHandler, clientName);
                foreach (var topic in topicsSubscribed)
                {
                    Console.WriteLine("{0}", topic);
                }
            }
            catch
            {
                Console.WriteLine("Not able to get the subscribed topics");
            }
        }

        private void getPulledMessages()
        {
            try
            {
                Queue<string> pulledMessages = subscriberService.pullMessages(networkHandler, clientName);
                Console.WriteLine("\n\nPulled Messages");
                foreach (var message in pulledMessages)
                {
                    Console.WriteLine("{0}", message);
                }
            }
            catch
            {
                Console.WriteLine("Not able to get the pulled messages");
            }
        }

        private void publishMessage()
        {
            try
            {
                publisherService.publishMessage(networkHandler);
            }
            catch
            {
                Console.WriteLine("Not able to get the publish message");
            }
        }

        private void createTopic()
        {
            try
            {
                publisherService.createTopic(networkHandler);
            }
            catch
            {
                Console.WriteLine("Not able to create Topic");
            }

        }

        private void subscribeToTopic()
        {
            try
            {
                subscriberService.subscribeToTopic(networkHandler, clientName);
            }
            catch
            {
                Console.WriteLine("Not able to subscribe to topic");
            }

        }

        private void createClient()
        {
            try
            {
                publisherService.createClient(networkHandler);
            }
            catch
            {
                Console.WriteLine("Not able to create client");
            }
        }

    }
}