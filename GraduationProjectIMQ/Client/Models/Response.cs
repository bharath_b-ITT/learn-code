using System;
using System.Collections.Generic;
namespace IMQ.Models
{
    public class Response
    {
        public string _responseMessage;
        public Queue<string> _data;
        public string responseMessage
        {
            get { return _responseMessage; }
            set { _responseMessage = value; }
        }

        public Queue<string> data
        {
            get { return _data; }
            set { _data = value; }
        }
    }
}
