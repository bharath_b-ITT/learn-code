﻿using System;
using System.Net;
namespace Client
{
    class Client
    {
        static void Main(string[] args)
        {
            ClientController clientController = new ClientController();
            try
            {
                clientController.connectToServer();
            }
            catch
            {
                System.Console.WriteLine("Not able to connect to Server");
            }
        }
    }
}