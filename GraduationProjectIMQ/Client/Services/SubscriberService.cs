using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using IMQ.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class SubscriberService
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IConfiguration config;
        private ClientConstants clientConstants;
        private IMQCommandsService imqCommandsService;
        public SubscriberService()
        {
            clientConstants = new ClientConstants();
            imqCommandsService = new IMQCommandsService();
        }
        public Queue<string> getTopicsWhichUserSubscribed(NetworkHandler networkHandler, string clientName)
        {
            sendMessageForGettingSubscribedTopics(networkHandler, clientName);
            Queue<string> receivedTopics = receiveTopicsFromServer(networkHandler);
            return receivedTopics;
        }

        public void sendMessageForGettingSubscribedTopics(NetworkHandler networkHandler, string clientName)
        {
            Request request = new Request();
            request.action = clientConstants.getSubscribedTopic;
            request.clientName = clientName;
            networkHandler.writeToServer(request);
        }
        public Queue<string> receiveTopicsFromServer(NetworkHandler networkHandler)
        {
            Response response = networkHandler.readFromServer();
            Queue<string> receivedTopics = response.data;
            return receivedTopics;
        }

        public string receiveResponseFromServer(NetworkHandler networkHandler)
        {
            Response response = networkHandler.readFromServer();
            string receivedMessage = response.responseMessage;
            Console.WriteLine("\nResponse From Server: {0}\n", receivedMessage);
            return receivedMessage;
        }

        public Queue<string> pullMessages(NetworkHandler networkHandler, string clientName)
        {
            string userCommand = getUserInput();
            IMQCommandStructure commandDetails = imqCommandsService.getIMQCommandDetails(userCommand);
            string topic = commandDetails.topic;
            sendTopicToPullMessages(networkHandler, topic, clientName);
            Queue<string> receivedMessages = getPulledMessagesFromServer(networkHandler);
            return receivedMessages;

        }

        public void sendTopicToPullMessages(NetworkHandler networkHandler, string topic, string clientName)
        {
            Request request = new Request();
            request.action = clientConstants.pullMessages;
            request.clientName = clientName;
            request.topic = topic;
            networkHandler.writeToServer(request);
        }

        public Queue<string> getPulledMessagesFromServer(NetworkHandler networkHandler)
        {
            Response response = networkHandler.readFromServer();
            Queue<string> receivedMessages = response.data;
            return receivedMessages;
        }

        public void subscribeToTopic(NetworkHandler networkHandler, string clientName)
        {
            string userCommand = getUserInput();
            IMQCommandStructure commandDetails = imqCommandsService.getIMQCommandDetails(userCommand);
            string topic = commandDetails.topic;
            sendTopicToSubscribe(networkHandler, topic, clientName);
            receiveResponseFromServer(networkHandler);
        }

        public void sendTopicToSubscribe(NetworkHandler networkHandler, string topic, string clientName)
        {
            Request request = new Request();
            request.action = clientConstants.subscribeTopic;
            request.topic = topic;
            request.clientName = clientName;
            networkHandler.writeToServer(request);
        }

        public string getUserInput()
        {
            Console.WriteLine("Enter the IMQ Command ");
            string command = Console.ReadLine();
            return command;
        }

    }
}