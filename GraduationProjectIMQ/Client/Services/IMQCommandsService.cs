using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using IMQ.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class IMQCommandsService
    {

        private IMQCommandStructure imqCommands;
        public IMQCommandsService()
        {
            imqCommands = new IMQCommandStructure();
        }

        public IMQCommandStructure getIMQCommandDetails(string IMQCommand)
        {
            try
            {

                string[] commandDetails = IMQCommand.Split(" ");
                string topic = commandDetails.Length >= 3 ? commandDetails[2] : "";
                string message = commandDetails.Length >= 4 ? commandDetails[3] : "";
                imqCommands.topic = topic;
                imqCommands.message = message;
                return imqCommands;
            }
            catch
            {
                Console.WriteLine("IMQ command is not valid");
                throw new Exception();

            }

        }
    }
}