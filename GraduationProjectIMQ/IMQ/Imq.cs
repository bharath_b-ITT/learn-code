﻿using System;
using System.Net;
using IMQ.Services;
using IMQ.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IMQ.Exceptions;
namespace IMQ
{
    class IMQ
    {
        static void Main(string[] args)
        {
            try
            {
                var builder = new ConfigurationBuilder();
                Startup.BuildConfig(builder);
                var host = Startup.CreateHostBuilder(args).Build();
                var imqServerController = ActivatorUtilities.CreateInstance<ServerController>(host.Services);
                imqServerController.startServer();
            }
                            catch(FailedToConnectClient error){
                    System.Console.WriteLine(error.Message);
                }
        }
    }
}
