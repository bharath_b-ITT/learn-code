using System;
namespace IMQ.Models
{
    public class DeadMessageModel
    {
        public int _topicId;
        public string _message;

        public int topicId
        {
            get { return _topicId; }
            set { _topicId = value; }
        }
        public string message
        {
            get { return _message; }
            set { _message = value; }
        }
    }
}