﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using IMQ;
using IMQ.Models;
using IMQ.Exceptions;

namespace IMQ.Repositories
{
    public class PublisherRepository : IPublisherRepository
    {
        private SqlConnection DbConnection;
        private ServerConstants serverConstants;
        public PublisherRepository()
        {
            serverConstants = new ServerConstants();
            initializeDbConnection();
        }
        public void initializeDbConnection()
        {
            try
            {
                var serverConstants = new ServerConstants();
                string connectionString = serverConstants.connectionString;
                SqlConnection connection = new SqlConnection(connectionString);
                this.DbConnection = connection;
                DbConnection.Open();
            }
            catch
            {
                Console.WriteLine("\n Not able initialize db connection\n");

            }
        }
        private int getTopicId(string topic)
        {
            try
            {
                string query = PubisherDbQueris.getTopicId;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topic", topic);
                SqlDataReader reader = command.ExecuteReader();
                int topicId = 0;
                while (reader.Read())
                {
                    topicId = ((int)reader["Id"]);
                }
                reader.Close();
                return topicId;
            }
            catch
            {
                Console.WriteLine("\n Not able get topic id from db\n");
                throw new Exception();
            }
        }
        public void savePublisherMessage(string topic, string message)
        {
            try
            {
                int topicId = getTopicId(topic);
                var time = DateTime.Now;
                string query = PubisherDbQueris.savePublisherMessage;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@message", message);
                command.Parameters.AddWithValue("@topicId", topicId);
                command.Parameters.AddWithValue("@time", time);
                command.ExecuteNonQuery();
            }
            catch
            {
                throw new FailedPublishingMessageException();

            }
        }
        public void handleDeadMessages(string topic)
        {
            try
            {
                int topicId = getTopicId(topic);
                Queue<DeadMessageModel> messages = checkForExpiredMessages(topicId);
                addDeadMessages(messages);
                deleteDeadMessages(topicId);
            }
            catch
            {
                throw new FailedToHandleDeadMessageException();

            }
        }
        public Queue<DeadMessageModel> checkForExpiredMessages(int topicId)
        {
            try
            {
                DateTime currentDate = DateTime.Now;
                string query = PubisherDbQueris.checkForExpiredMessages;
                Queue<DeadMessageModel> deadMessages = new Queue<DeadMessageModel>();
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topicId", topicId);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    DeadMessageModel deadMessage = new DeadMessageModel();
                    string message = ((string)reader["Message"]);
                    int topicID = ((int)reader["TopicId"]);
                    deadMessage.message = message;
                    deadMessage.topicId = topicID;
                    deadMessages.Enqueue(deadMessage);
                }
                reader.Close();
                return deadMessages;
            }
            catch
            {
                Console.WriteLine("\n Not able check expired messages from db\n");
                throw new Exception();
            }
        }

        public void addDeadMessages(Queue<DeadMessageModel> messages)
        {
            try
            {
                foreach (DeadMessageModel message in messages)
                {
                    string query = PubisherDbQueris.addDeadMessages;
                    SqlCommand command = new SqlCommand(query, DbConnection);
                    command.Parameters.AddWithValue("@message", message.message);
                    command.Parameters.AddWithValue("@topicId", message.topicId);
                    command.ExecuteNonQuery();
                }
            }
            catch
            {
                Console.WriteLine("\n Not able add topics to db\n");

            }
        }
        public void deleteDeadMessages(int topicId)
        {
            try
            {
                string query = PubisherDbQueris.deleteMessages;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topicId", topicId);
                command.ExecuteNonQuery();
            }
            catch
            {
                Console.WriteLine("\n Not able delete dead messages from db\n");

            }
        }
        public void createTopic(string topicName)
        {
            try
            {
                string query = PubisherDbQueris.createTopic;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topicName", topicName);
                command.ExecuteNonQuery();
            }
            catch
            {
                throw new FailedToCreateTopicException();

            }
        }
    }
}