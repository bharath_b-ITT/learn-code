using System;
using System.Collections.Generic;
using IMQ.Models;

namespace IMQ.Repositories
{
    public interface IPublisherRepository
    {
        void initializeDbConnection();
        void savePublisherMessage(string topic, string message);
        void createTopic(string topicName);
        void handleDeadMessages(string topic);
        Queue<DeadMessageModel> checkForExpiredMessages(int topicId);
        void addDeadMessages(Queue<DeadMessageModel> messages);
        void deleteDeadMessages(int topicId);

    }
}