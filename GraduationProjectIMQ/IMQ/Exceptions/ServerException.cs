using System;
namespace IMQ.Exceptions
{
    public class FailedToStartServerException : System.IO.IOException
    {
        public FailedToStartServerException() : base("Not able to start server")
        {
        }
    }
    public class MaximumClientsException : Exception
    {
        public MaximumClientsException() : base("Maximum number of clients reached")
        {
        }
    }

    public class FailedToConnectClient : Exception
    {
        public FailedToConnectClient() : base("Not able to connect to client")
        {
        }
    }

    public class FailedInitiliazingServer : Exception
    {
        public FailedInitiliazingServer() : base("Not able to initialize Server")
        {
        }
    }



}
