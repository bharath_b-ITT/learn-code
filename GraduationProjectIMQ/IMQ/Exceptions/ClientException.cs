using System;
namespace IMQ.Exceptions
{
    public class ClientDisconnectedException : System.IO.IOException
    {
        public ClientDisconnectedException() : base("Client Disconnected Unexpectedly")
        {
        }
    }
    public class ClientDoesNotExistsException : Exception
    {
        public ClientDoesNotExistsException() : base("Client Does Not Exists")
        {
        }
    }

    public class FailedInitializingStreamException : Exception
    {
        public FailedInitializingStreamException() : base("Not able to initialize stream")
        {
        }
    }

    public class FailedSendingTopicsException : Exception
    {
        public FailedSendingTopicsException() : base("Not able to send all the topics")
        {
        }
    }

    public class FailedCreatingClientException : Exception
    {
        public FailedCreatingClientException() : base("Not able to create client")
        {
        }
    }

    public class FailedAuthenticatingException : Exception
    {
        public FailedAuthenticatingException() : base("Not able to authenticate client")
        {
        }
    }
    public class FailedSendingResponseException : Exception
    {
        public FailedSendingResponseException() : base("Not able to send response to client")
        {
        }
    }

}
