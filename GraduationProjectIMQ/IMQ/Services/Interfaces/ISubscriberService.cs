using System;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;
namespace IMQ.Services
{
    public interface ISubscriberService
    {
        void sendResponseToClient(NetworkStream stream, Response response);
        public void getAllSubscribedTopics(NetworkStream stream, Request request);
        public void pullMessagesOfSpecifcTopic(NetworkStream stream, Request request);
        public void subscribeToNewTopic(NetworkStream stream, Request request);

    }
}