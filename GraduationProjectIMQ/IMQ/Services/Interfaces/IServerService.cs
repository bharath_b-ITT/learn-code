using System;

namespace IMQ.Services
{
    public interface IServerService
    {
        void startServer();
        void connectToClient();
    }
}