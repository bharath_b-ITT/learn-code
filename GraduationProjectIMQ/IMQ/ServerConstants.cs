using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace IMQ
{
    public class ServerConstants
    {
        public ServerConstants()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            IConfiguration config = builder.Build();
            this.ipAddress = config["ServerConfig:IPAddress"];
            this.portNumber = int.Parse(config["ServerConfig:Port"]);
            this.connectionString = config["ConnectionStrings:DbConnection"];
            this.getAllTopic = config["actions:getAllTopic"];
            this.publishMessage = config["actions:publishMessage"];
            this.getSubscribedTopic = config["actions:getSubscribedTopic"];
            this.pullMessages = config["actions:pullMessages"];
            this.subscribeTopic = config["actions:subscribeTopic"];
            this.register = config["actions:register"];
            this.authenticate = config["actions:authenticate"];
            this.publisher = config["roles:publisher"];
            this.subscriber = config["roles:subscriber"];
            this.publishedMessage = config["responseMessages:publishedMessage"];
            this.createdTopic = config["responseMessages:createdTopic"];
            this.subscribedToTopic = config["responseMessages:subscribedToTopic"];
            this.registeredSuccessfully = config["responseMessages:registeredSuccessfully"];
        }
        public String ipAddress;
        public Int32 portNumber;
        public string connectionString;
        public string getAllTopic;
        public string publishMessage;
        public string getSubscribedTopic;
        public string createTopic;
        public string pullMessages;
        public string subscribeTopic;
        public string register;
        public string authenticate;
        public string publisher;
        public string subscriber;

        public string publishedMessage;
        public string createdTopic;
        public string subscribedToTopic;
        public string registeredSuccessfully;

    }
}