using System.Collections.Immutable;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;
using IMQ.Controllers;
using IMQ.Services;

using Newtonsoft.Json;
namespace IMQ.Commands
{
  static class IMQCommands
{
        public const string getAllTopic ="getAllTopic" ;
        public const string publishMessage = "publishMessage";
        public const string getSubscribedTopic= "getSubscribedTopic";
        public const string createTopic= "createTopic";
        public const string pullMessages= "pullMessages";
        public const string subscribeTopic= "subscribeTopic";
        public const string register= "register";
        public const string authenticate= "authenticate";

}

    }