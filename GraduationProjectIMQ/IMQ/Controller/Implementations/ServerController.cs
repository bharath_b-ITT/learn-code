using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using IMQ.Services;
using IMQ.Controllers;
using IMQ.Exceptions;

namespace IMQ.Controllers
{
    class ServerController : IServerController
    {
        private TcpListener server;
        private TcpClient client;
        private string ipAddress;
        private Int32 portNumber;
        private ServerService serverService;

        public ServerController(IServerService serverService)
        {
            this.serverService = (ServerService)serverService;
        }

        public void startServer()
        {
            try
            {
                this.serverService.startServer();
            }
            catch
            {
                System.Console.WriteLine("not able to start server");
            }
        }
    }
}