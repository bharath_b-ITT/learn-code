using System;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;

namespace IMQ.Controllers
{
    public interface IClientController
    {
        public void initializeNetworkStreams();
        public void connectToClient(TcpClient _client);
    }
}