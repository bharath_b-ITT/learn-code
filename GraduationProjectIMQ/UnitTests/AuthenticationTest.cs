using System;
using Xunit;
using IMQ.Services;
using Moq;
using IMQ.Models;
using IMQ.Tests;
using IMQ.Repositories;

namespace IMQ.Login.Tests
{
    public class AuthenticationTest
    {

        [Fact]
        public void verifyPublisher_shouldPass()
        {
            var mockClientRepository = new Mock<IClientRepository>();
            var mockPublisherService = new Mock<IPublisherService>();
            var mockSubscriberService = new Mock<ISubscriberService>();
            var authRequest = new Request();
            authRequest.role = "publisher";
            authRequest.clientName = "testpublisher";
            authRequest.password="abc";
            mockClientRepository.Setup(repository => repository.verifyClientCredentials(authRequest.clientName, authRequest.role, authRequest.password));
            var clientService = new ClientService(mockClientRepository.Object, mockPublisherService.Object, mockSubscriberService.Object);
            clientService.authenticateClient(authRequest.clientName, authRequest.role, authRequest.password);
            mockClientRepository.Verify(repository => repository.verifyClientCredentials(authRequest.clientName, authRequest.role, authRequest.password), Times.Exactly(1));
        }

        [Fact]
        public void verifySubscriber_shouldPass()
        {
            var mockClientRepository = new Mock<IClientRepository>();
            var mockPublisherService = new Mock<IPublisherService>();
            var mockSubscriberService = new Mock<ISubscriberService>();
            var authRequest = new Request();
            authRequest.role = "subscriber";
            authRequest.clientName = "testsubscriber";
            authRequest.password="abc";
            mockClientRepository.Setup(repository => repository.verifyClientCredentials(authRequest.clientName, authRequest.role, authRequest.password));
            var clientService = new ClientService(mockClientRepository.Object, mockPublisherService.Object, mockSubscriberService.Object);
            clientService.authenticateClient(authRequest.clientName, authRequest.role, authRequest.password);
            mockClientRepository.Verify(repository => repository.verifyClientCredentials(authRequest.clientName, authRequest.role, authRequest.password), Times.Exactly(1));
        }


    }
}
